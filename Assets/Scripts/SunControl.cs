﻿using UnityEngine;
using System.Collections;

public class SunControl : MonoBehaviour {

	//Range for min/max values of variable
	[Range(-10f, 10f)]
	public float sunRotationSpeed_x, sunRotationSpeed_y;
	[SerializeField]
	float maxXrotation = 170;
	[SerializeField]
	float minXrotation = 20;
	

	// Sun Movement
	void Update () {

		if (gameObject.transform.rotation.eulerAngles.x > maxXrotation || gameObject.transform.rotation.eulerAngles.x < minXrotation)
		{
			sunRotationSpeed_x = -sunRotationSpeed_x;
		}

		gameObject.transform.Rotate (sunRotationSpeed_x * Time.deltaTime, sunRotationSpeed_y * Time.deltaTime, 0);
	}
}
