﻿using System;

namespace Models
{
	[Serializable]
	public class LoginResponse
	{
		public User data;
		public bool ok;
		public string token;
		public string error;

		public override string ToString()
		{
			return UnityEngine.JsonUtility.ToJson(this, true);
		}
	}
}

