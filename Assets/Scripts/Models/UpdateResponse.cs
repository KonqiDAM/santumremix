﻿using System;

namespace Models
{
	[Serializable]
	public class UpdateResponse
	{
		public bool ok;

		public override string ToString()
		{
			return UnityEngine.JsonUtility.ToJson(this, true);
		}
	}
}
