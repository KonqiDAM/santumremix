﻿using System;

namespace Models
{
	[Serializable]
	public class Stats
	{
		public int timePlayed;
		public int weaponPrecision;
		public int roundsPlayed;
		public int totalDeaths;
		public int totalKills;
		public int totalBulletsUsed;
		public override string ToString()
		{
			return UnityEngine.JsonUtility.ToJson(this, true);
		}
	}
}

