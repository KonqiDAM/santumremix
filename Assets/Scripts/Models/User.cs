﻿using System;

namespace Models
{
	[Serializable]
	public class User
	{
		public string name;
		public string password;
		public Stats stats;
		public override string ToString(){
			return UnityEngine.JsonUtility.ToJson (this, true);
		}
	}
}

