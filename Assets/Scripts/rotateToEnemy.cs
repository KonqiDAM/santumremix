﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateToEnemy : MonoBehaviour
{
    GameObject target = null;
    [SerializeField]
    float speed = 3;
    [SerializeField]
    float shootDistance = 3;
    List<GameObject> enemies;
    turretShoot shootSc;

    void Start()
    {
        shootSc = this.GetComponent<turretShoot>();
        
    }

    // Update is called once per frame
    void Update()
    {
        selectTarget();
        RotateToEnemy();
    }

    void selectTarget()
    {
        enemies = FindObjectOfType<Controller>().enemies;

        if (target != null && !target.GetComponent<Enemy>().isAlive())
        {
            target = null;
            shootSc.stopShooting();
        }

        if (target == null && enemies.Count > 1)
        {
            target = enemies[0];
            foreach (GameObject e in enemies)
            {
                if(target.GetComponent<Enemy>().isAlive())
                    if (Vector3.Distance(e.transform.position, transform.position) < Vector3.Distance(target.transform.position, transform.position))
                        target = e;
            }
        }
        else if (enemies.Count == 1 )
            target = enemies[0];
        else if (enemies.Count == 0)
            target = null;
        if (target != null && Vector3.Distance(target.transform.position, transform.position) > shootDistance)
            target = null;
    }

    void RotateToEnemy()
    {
        float lastX = this.transform.rotation.x;
        if (target != null && target.GetComponent<Enemy>().isAlive())
        {
            // Determine which direction to rotate towards
            Vector3 targetDirection = target.transform.position - transform.position;

            // The step size is equal to speed times frame time.
            float singleStep = speed * Time.deltaTime;

            // Rotate the forward vector towards the target direction by one step
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, singleStep, 0.0f);

            // Draw a ray pointing at our target in
            Debug.DrawRay(transform.position, newDirection, Color.red);
            float enemyAngle = Vector3.Angle(this.transform.forward, transform.position - target.transform.position);
            if (enemyAngle < 185 && enemyAngle > 175)
            {
                shootSc.activateShoot(target);
            }
            else
                shootSc.stopShooting();
            // Calculate a rotation a step closer to the target and applies rotation to this object
            this.transform.rotation = Quaternion.LookRotation(newDirection);

        }
        else
        {
            float singleStep = speed * Time.deltaTime;
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, new Vector3(-999990, 0, 0) - transform.position, singleStep, 0.0f);
            this.transform.rotation = Quaternion.LookRotation(newDirection);
        }

        /*
        if (this.transform.rotation.x > 0.20 || this.transform.rotation.x < 0.20)
            this.transform.rotation = new Quaternion(lastX, this.transform.rotation.y, this.transform.rotation.z, this.transform.rotation.w);
            */
    }
}
