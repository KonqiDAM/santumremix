﻿using Proyecto26;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Models;
using TMPro;

public class StatsUpdater : MonoBehaviour
{
    private readonly string basePath = "http://localhost:3000";
    private RequestHelper currentRequest;
    public string token = "";
    public Stats stats;
    public User user;
    private double startTime;

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
        if (FindObjectsOfType<StatsUpdater>().Length > 1 && token == "")
            Destroy(this.gameObject);
    }

    public void updateStats()
    {
        stats.timePlayed += (int) (Time.time - startTime);
        startTimeCount();
        currentRequest = new RequestHelper
        {

            Uri = basePath + "/stats",
            Headers = new Dictionary<string, string> {
                { "authorization", token }
            },
            Body = stats,
            Retries = 5,
            RetrySecondsDelay = 1,
            RetryCallback = (err, retries) => {
                Debug.Log(string.Format("Retry #{0} Status {1}\nError: {2}", retries, err.StatusCode, err));
            }
        };

        RestClient.Put<UpdateResponse>(currentRequest, (err, res, body) => {
            if (err != null)
            {
                Debug.Log("Error " + err.Message);
            }
            else
            {
                //Debug.Log("BIEN " + JsonUtility.ToJson(body, true));
            }
        });
    }

    public void startTimeCount()
    {
        startTime = Time.time;
    }

}
