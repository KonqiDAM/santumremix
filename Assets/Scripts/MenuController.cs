﻿using Proyecto26;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Models;
using TMPro;

public class MenuController : MonoBehaviour
{
    [SerializeField]
    InputField userName = null;
    [SerializeField]
    InputField password = null;
    [SerializeField]
    GameObject panelLogin = null;
    [SerializeField]GameObject panelErrors = null;
    [SerializeField]
    TextMeshProUGUI textErrors = null;
    [SerializeField]
    GameObject panelStats = null;
    [SerializeField]
    TextMeshProUGUI textStats = null;
    private readonly string basePath = "localhost:3000";
    private RequestHelper currentRequest;
    StatsUpdater statsUpdater;

    private void LogMessage(string title, string message)
    {
        #if UNITY_EDITOR
            EditorUtility.DisplayDialog(title, message, "Ok");
        #else
		    Debug.Log(message);
        #endif
    }

    // Start is called before the first frame update
    void Start()
    {
        password.inputType = InputField.InputType.Password;
        statsUpdater = FindObjectOfType<StatsUpdater>();
        if(statsUpdater.user != null && statsUpdater.user.name != "")
        {
            userName.text = statsUpdater.user.name;
            password.text = statsUpdater.user.password;
            login();
        }
        Cursor.lockState = CursorLockMode.Confined;


    }

    // Update is called once per frame
    void Update()
    {
        //password.text = new string('*', password.text.Length);
    }

    public bool checkCredentialsLength()
    {
        if (userName.text.ToString().Length < 4)
        {
            panelErrors.SetActive(true);
            textErrors.text = "Username must be 4 chars long or more!";
            textErrors.color = Color.red;
            return false;

        }
        else if (password.text.ToString().Length < 4)
        {
            panelErrors.SetActive(true);
            textErrors.text = "Password must be 4 chars long or more!";
            textErrors.color = Color.red;
            return false;
        }
        panelErrors.SetActive(false);

        return true;
        }

    public void login()
    {
        if (checkCredentialsLength())
        {

            statsUpdater.user = new User { name = userName.text.ToString(), password = password.text.ToString() };

            currentRequest = new RequestHelper
            {
                Uri = basePath + "/login",
                Body = new User
                {
                    name = userName.text.ToString(),
                    password = password.text.ToString()
                },
                EnableDebug = true
            };

            RestClient.Post<LoginResponse>(currentRequest)
            .Then(res =>
            {
                RestClient.ClearDefaultParams();
            //this.LogMessage("Success", JsonUtility.ToJson(res, true));
            if (res.ok)
                {
                    
                    panelLogin.SetActive(false);
                    panelErrors.SetActive(false);
                    panelStats.SetActive(true);
                    int auxTimePlayed = System.Convert.ToInt32(res.data.stats.timePlayed);
                    float auxTime = auxTimePlayed > 60 ? auxTimePlayed / 60f : auxTimePlayed;
                    textStats.text = @"User: " + res.data.name.ToString()
                        + "\nShooted bullets: " + res.data.stats.totalBulletsUsed
                        + "\nPrecision: " + (res.data.stats.totalBulletsUsed > 0 ? ((100.0f * res.data.stats.weaponPrecision / (float)res.data.stats.totalBulletsUsed).ToString("0.0") + " %" ): "No data")
                        + "\nRounds played: " + res.data.stats.roundsPlayed
                        + "\nDeaths: " + res.data.stats.totalDeaths
                        + "\nKills: " + res.data.stats.totalKills
                        + "\nPlay time: " + auxTime.ToString("0") + (auxTimePlayed > 60 ? "M" : "S" );
                    statsUpdater.token = res.token;
                    statsUpdater.stats = res.data.stats;
                    
                }
                else
                {
                //Debug.Log(res.error);
                panelErrors.SetActive(true);
                    textErrors.text = res.error;
                    textErrors.color = Color.red;
                }
            })
            .Catch(err => this.LogMessage("Error", err.Message));
        }
    }
    public void register()
    {
        if(checkCredentialsLength())
        {
            currentRequest = new RequestHelper
            {
                Uri = basePath + "/register",
                Body = new User
                {
                    name = userName.text.ToString(),
                    password = password.text.ToString()
                },
                EnableDebug = false
            };

            RestClient.Post<LoginResponse>(currentRequest)
            .Then(res =>
            {
                RestClient.ClearDefaultParams();
            //this.LogMessage("Success", JsonUtility.ToJson(res, true));
            if (res.ok)
                { 
                    panelErrors.SetActive(true);
                    textErrors.text = "User created";
                    textErrors.color = Color.white;
                
            }
                else
                {
                //Debug.Log(res.error);
                panelErrors.SetActive(true);
                    textErrors.text = res.error;
                    textErrors.color = Color.red;
                }
            })
            .Catch(err => this.LogMessage("Error", err.Message));
        }
    }
    public void logout()
    {
        textStats.text = "";
        statsUpdater.token = "";
        statsUpdater.user.name = "";
        statsUpdater.user.password = "";
        panelLogin.SetActive(true);
        panelErrors.SetActive(false);
        panelStats.SetActive(false);
    }
    public void start()
    {
        SceneManager.LoadScene("Level1");
    }
    public void exit()
    {
        Application.Quit();
    }

    [SerializeField]
    GameObject creditsPannel;
    public void unHideCreditsPannel()
    {
        if (creditsPannel.activeSelf)
            creditsPannel.SetActive(false);
        else
            creditsPannel.SetActive(true);
    }
}