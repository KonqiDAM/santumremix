﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    float speed = 3;
    int actualPoint = 0;
    List<Point> points;
    float timeToStart = 1000;
    public int hitPoints = 30;
    [SerializeField]
    Vector3 aimOffset = new Vector3(0, 1, 0);

    bool started = false;


    public void initialize(List<Point> points, float timeToStart = 0)
    {
        this.timeToStart = timeToStart;
        this.points = points;


        this.transform.position = new Vector3(-10, -10, -10);
    }

    // Update is called once per frame
    void Update()
    {
        if (FindObjectOfType<Controller>().gamePaused)
            return;

        if (!started)
        {
            timeToStart -= Time.deltaTime;
            if (timeToStart <= 0)
            {
                started = true;
                this.transform.position = points[0].getVector3();
            }
        }
        float step = speed * Time.deltaTime; // calculate distance to move

        if (timeToStart <= 0 && actualPoint < points.Count && hitPoints > 0)
        {
            this.transform.position = Vector3.MoveTowards(transform.position, points[actualPoint].getVector3(), step);
            if (Vector3.Distance(transform.position, points[actualPoint].getVector3()) < 0.001f)
            {
                actualPoint++;

                if (actualPoint == points.Count)
                {
                    damageCore();
                    return;
                }
            }
            rotateToPlatform();
        }
        //If enemy is dead and "Die" animation is over
        else if (hitPoints <= 0 && this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
        {
            transform.position += Vector3.down * Time.deltaTime;
        }
    }

    void rotateToPlatform()
    {
        // Determine which direction to rotate towards
        Vector3 targetDirection = points[actualPoint].getVector3() - transform.position;

        // The step size is equal to speed times frame time.
        float singleStep = speed * Time.deltaTime;

        // Rotate the forward vector towards the target direction by one step
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, singleStep, 0.0f);

        // Draw a ray pointing at our target in
        Debug.DrawRay(transform.position, newDirection, Color.red);

        // Calculate a rotation a step closer to the target and applies rotation to this object
        transform.rotation = Quaternion.LookRotation(newDirection);
    }

    public void Hit(int amount)
    {
        hitPoints -= amount;
        //if enemie dies, execute his animation
        if (hitPoints <= 0 && !GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Die"))
        {
            this.GetComponent<Animator>().Play("Die");
            FindObjectOfType<StatsUpdater>().stats.totalKills++;

            Destroy(this.gameObject, 3);
        }
    }

    private void damageCore()
    {
        FindObjectOfType<Controller>().DamageCore(10);
        Destroy(this.gameObject);
    }

    public Vector3 getAimOffset()
    {
        return aimOffset;
    }

    public bool isAlive()
    {
        return hitPoints > 0;
    }
}
