﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turretShoot : MonoBehaviour
{
    public GameObject[] muzzelFlash;
    public GameObject muzzelSpawn;
    public GameObject muzzelSpawn2 = null;
    private GameObject holdFlash;
    private GameObject holdFlash2;

    [SerializeField]
    float timeBetweenShoots = 2;
    float lastShootTime = 2;

    [SerializeField]
    GameObject bullet = null;

    bool canShoot = false;
    GameObject target = null;

    // Start is called before the first frame update
    void Start()
    {
        if (FindObjectOfType<Controller>().gamePaused)
            return;

        int randomNumberForMuzzelFlash = Random.Range(0, muzzelFlash.Length);
        holdFlash = Instantiate(muzzelFlash[randomNumberForMuzzelFlash], muzzelSpawn.transform.position, Quaternion.Euler(0, -90, 0)) as GameObject;
        holdFlash.transform.parent = muzzelSpawn.transform;
        muzzelSpawn.SetActive(false);
        if(muzzelSpawn2 != null)
        {
            holdFlash2 = Instantiate(muzzelFlash[randomNumberForMuzzelFlash], muzzelSpawn2.transform.position, Quaternion.Euler(0, -90, 0)) as GameObject;
            holdFlash2.transform.parent = muzzelSpawn2.transform;
            muzzelSpawn2.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        if(target != null && canShoot && target.GetComponent<Enemy>().isAlive())
        { 
            lastShootTime -= Time.deltaTime;
            if (lastShootTime <= 0 )
            {
                lastShootTime = timeBetweenShoots;
                if(target != null)
                    shot();
            }
        }
    }

    void shot()
    {
        muzzelSpawn.SetActive(true);
        if (muzzelSpawn2)
            muzzelSpawn2.SetActive(true);
        if (bullet)
        {
            GameObject newBullet = Instantiate(bullet, muzzelSpawn.transform.position, muzzelSpawn.transform.rotation);
            newBullet.SendMessage("startBullet", target);
        }
        StartCoroutine(deactivateMuzzle());
    }

    IEnumerator deactivateMuzzle()
    {
        yield return new WaitForSeconds(0.1f);
        muzzelSpawn.SetActive(false);
        if(muzzelSpawn2)
            muzzelSpawn2.SetActive(false);
    }

    public void activateShoot(GameObject target)
    {
        canShoot = true;
        this.target = target;
    }

    public void stopShooting()
    {
        canShoot = false;
        this.target = null;
    }
}
