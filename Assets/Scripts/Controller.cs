﻿using cakeslice;
using Models;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{
    const int mapWidth = 8;
    const int mapHeight = 8;
    const char BASE = 'B';
    const char NONE = '0';
    const char SPAWN = 'S';
    const char PLATFORM = '1';
    const char NORMALTURRET = '2';
    const char FASTTURRET = '3';
    const char FIRETURRET = '4';
    const char AUXFILLER = '9';
    public Point SPAWNPOSITION = new Point(0, 0);
    public static Point BASEPOSITION = new Point(mapWidth - 1, mapHeight - 1);

    ConstrcSelector cnstSelector;
    public List<GameObject> platforms;
    public List<GameObject> turrets;
    public List<GameObject> enemies;
    public List<Point> points;

    public bool isConstructingMode = true;
    public int money = 300;
    public int remainingPlatforms = 4;
    public Vector3 lastPosition;
    public const float floorHeight = 0.4f;
    public const int tileSize = 4;

    public bool gamePaused = false;
    public bool isConstructingPlatform = false;
    public bool isDeletingPlatform = false;
    public float timeLeftToConstruct;
    public float timeLeftToDelete;

    public int actualSelectedTurret = 1;
    public int actualCoreHealth = 150;
    public int maxCoreHealth = 150;
    [SerializeField]
    float subitCooldownMax = 0.5f;
    float submitCooldown;
    float vignetteActualVariation = 0;
    float vignetteMaxVariation = 0.1f;
    float vignetteJumpsVariation = 0.0005f;


    public Vector3 lastPointingPosition;
    public GameObject platformToDelete;
    public GameObject turretToDelete;


    [SerializeField]
    GameObject panelPauseMenu;
    [SerializeField]
    Text textRoundInformation = null;
    int actualRound = 1;
    int maxRound = 3;
    [SerializeField]
    public float timeToContruct = 1;
    [SerializeField]
    public float timeToDelete = 1;
    public GameObject constructionPlatform;
    [SerializeField]
    GameObject constructTurretFire = null;
    [SerializeField]
    GameObject constructTurretFast = null;
    [SerializeField]
    GameObject constructTurretNormal = null;
    [SerializeField]
    GameObject platformPrefab = null;
    [SerializeField]
    GameObject normalTurretPrefab = null;
    [SerializeField]
    GameObject fastTurretPrefab = null;
    [SerializeField]
    GameObject fireTurretPrefab = null;
    [SerializeField]
    GameObject grassFloor;
    [SerializeField]
    GameObject metallicFloor;
    [SerializeField]
    GameObject[] enemiesPrefabs = null;
    [SerializeField]
    PostProcessVolume m_PostProcessVolume = null;
    [SerializeField]
    float maxVignette = 0.6f;
    [SerializeField]
    float maxGrain = 0.6f;
    [SerializeField]
    int vignetteHealthThreshold=25;
    Vignette vignette;
    Grain grain;
    [SerializeField]
    Image winImage = null;
    [SerializeField]
    Image defeatImage = null;
    [SerializeField]
    AudioClip winSound = null;
    [SerializeField]
    AudioClip defeatSound = null;
    bool playerWin = false;
    bool playerDefeat = false;
    public AudioSource audioSource;
    StatsUpdater statsUpdater;
    private char[,] map;
    private char[,] auxMap;

    void Start()
    {
        Cursor.visible = false;
        textRoundInformation.text = "Round " + actualRound + "/" + maxRound;
        constructTurretFire.SetActive(false);
        constructTurretFast.SetActive(false);
        constructTurretNormal.SetActive(false);
        submitCooldown = subitCooldownMax;
        cnstSelector = FindObjectOfType<ConstrcSelector>();
        platforms = new List<GameObject>();
        map = new char[mapWidth, mapHeight];
        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight; j++)
            {
                map[i, j] = NONE;
            }
        }
        map[SPAWNPOSITION.x, SPAWNPOSITION.y] = SPAWN;
        map[BASEPOSITION.x, BASEPOSITION.y] = BASE;
        map[3, 0] = AUXFILLER;
        map[3, 1] = AUXFILLER;
        
        lastPosition = new Vector3(0, 0, 0);
        m_PostProcessVolume.profile.TryGetSettings(out vignette);
        m_PostProcessVolume.profile.TryGetSettings(out grain);
        points = new List<Point>();
        statsUpdater = FindObjectOfType<StatsUpdater>();
    }

    // Update is called once per frame
    void Update()
    {
        keysCheck();
        if (!gamePaused)
        {
            submitCheck();
            removeDeadEnemies();
        }
    }

    public void pauseGame()
    {
        Time.timeScale = 0.0001f;
        panelPauseMenu.SetActive(true);
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;

        gamePaused = true;
    }

    public void unPauseGame()
    {
        panelPauseMenu.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        gamePaused = false;
        Time.timeScale = 1f;
        Cursor.visible = false;

    }

    void keysCheck()
    {

        if(Input.GetKeyUp(KeyCode.F1))
        {
            enemies.ForEach(x => x.GetComponent<Enemy>().hitPoints = 0);
        }
        if (Input.GetKeyUp(KeyCode.F2))
        {
            actualCoreHealth = 0;
        }
        if(Input.GetKeyUp(KeyCode.Escape))
        {
            if (gamePaused)
                unPauseGame();
            else
                pauseGame();
        }
    }

    private void FixedUpdate()
    {
        if (gamePaused)
            return;

        if (playerWin)
            imageTransparency(winImage);
        else if (playerDefeat)
            imageTransparency(defeatImage);
        else
        {
            bloodEfect();
            if (!isConstructingMode)
                checkRoundEnded();
        }
    }

    private void imageTransparency(Image img)
    {
        var tempColor = img.color;

        if (img.color.a > 0.9f)
        {
            tempColor.a = 1f;
        }
        else
        {
            tempColor.a = img.color.a + Time.deltaTime / 4f;
        }
        img.color = tempColor;
    }
        
    private void checkRoundEnded()
    {
        if (enemies.Count == 0)
        {
            actualRound++;
            statsUpdater.stats.roundsPlayed++;
            cnstSelector.setEnabled();
            isConstructingMode = true;
            FindObjectOfType<GunInventory>().SendMessage("changeToConstructionWeapon");
            textRoundInformation.text = "Round " + actualRound + "/" + maxRound;
            if (actualRound > maxRound)
            {
                playerWin = true;
                audioSource.PlayOneShot(winSound);
                textRoundInformation.text = "You win!!!";
                StartCoroutine("changeToMainMenu", 12);
            }
            money += 300;
            remainingPlatforms += 5;

            if(statsUpdater.token != "")
                statsUpdater.updateStats();
        }
        if (actualCoreHealth <= 0)
        {
            checkCoreHealth();
            playerDefeat = true;
            audioSource.PlayOneShot(defeatSound);
            statsUpdater.stats.totalDeaths++;
            if (statsUpdater.token != "")
                statsUpdater.updateStats();
            StartCoroutine("changeToMainMenu", 8);
        }

    }

    void removeDeadEnemies()
    {
        if (!isConstructingMode)
        {
            enemies = enemies.Where(x => x != null).ToList();
            enemies = enemies.Where(x => x.GetComponent<Enemy>().isAlive()).ToList();
        }
    }

    private void bloodEfect()
    {
        if (vignetteHealthThreshold < actualCoreHealth)
            grain.intensity.value = 0;
        else
        {
            grain.intensity.value = (1 - (float)actualCoreHealth / maxCoreHealth) * maxGrain;

        }
        vignetteActualVariation += vignetteJumpsVariation;
        if (vignetteActualVariation > vignetteMaxVariation || vignetteActualVariation < 0)
        {
            vignetteJumpsVariation = -vignetteJumpsVariation;
        }
        vignette.intensity.value = (1 - (float)actualCoreHealth / maxCoreHealth) * maxVignette + vignetteActualVariation;
        
    }

    private void submitCheck()
    {


        if (submitCooldown > 0)
            submitCooldown -= Time.deltaTime;

        if (Input.GetAxis("Submit") != 0 && submitCooldown <= 0)
        {


            submitCooldown = subitCooldownMax;
            if (isConstructingMode)
            {
                textRoundInformation.text = "Core " + actualCoreHealth + "/" + maxCoreHealth;

                points.Clear();
                auxMap = (char[,])map.Clone();
                tracePath(SPAWNPOSITION.x, SPAWNPOSITION.y);

                cnstSelector.setEnabled(false);
                constructionPlatform.SetActive(false);
                isConstructingMode = false;
                FindObjectOfType<GunInventory>().SendMessage("changeToGunMove");

                spawnEnemies();
            }
        }
    }

    private void spawnEnemies()
    {
        for (int i = 0; i < actualRound * 10; i++)
        {
            enemies.Add(Instantiate(enemiesPrefabs[Random.Range(0, 3)], new Vector3(), Quaternion.identity));

        }
        points.Add(SPAWNPOSITION);
        points.Reverse();

        int c = 0;
        foreach (GameObject e in enemies)
        {
            c += 1;
            e.GetComponent<Enemy>().initialize(new List<Point>(points), c);
        }
    }

    public void CheckConstructionPlatform()
    {
        if (lastPosition != constructionPlatform.transform.position)
        {
            constructionPlatform.transform.position = lastPosition;
        }
        if (!isConstructingMode)
            constructionPlatform.SetActive(false);
    }

    public void CreatePlatform()
    {
        remainingPlatforms--;

        int auxX = (int)lastPointingPosition.x / 4;
        int auxY = (int)lastPointingPosition.z / 4;
        if (map[auxX, auxY] == '0' && Mathf.Approximately(lastPointingPosition.y, floorHeight) && constructionPlatform.activeSelf)
        {
            platforms.Add(Instantiate(platformPrefab, lastPointingPosition, Quaternion.identity));
            map[auxX, auxY] = (char)('0' + actualSelectedTurret);//0 + value and conver again to char
            switch(actualSelectedTurret)
            {
                case 2:
                    turrets.Add(Instantiate(normalTurretPrefab, constructTurretNormal.transform.position, Quaternion.identity));
                    break;
                case 3:
                    turrets.Add(Instantiate(fastTurretPrefab, constructTurretNormal.transform.position, Quaternion.identity));
                    break;
                case 4:
                    turrets.Add(Instantiate(fireTurretPrefab, constructTurretNormal.transform.position, Quaternion.identity));
                    break;
            }
            if (actualSelectedTurret != 1)
                money -= 100;
        }
        isConstructingPlatform = false;
    }

    public void DestroyTurret()
    {
        money += 100;

        int auxX = (int)platformToDelete.transform.position.x / 4;
        int auxZ = (int)platformToDelete.transform.position.z / 4;
        map[auxX, auxZ] = '1';
        turrets.Remove(turretToDelete);
        Destroy(turretToDelete);
    }

    public void DestroyPlatform()
    {

        if (platformToDelete != null)
        {

            int auxXmap = (int)platformToDelete.transform.position.x / 4;
            int auxZmap = (int)platformToDelete.transform.position.z / 4;
            if (map[auxXmap, auxZmap].Equals('2') || map[auxXmap, auxZmap].Equals('3') || map[auxXmap, auxZmap].Equals('4'))
            {
                turretToDelete = turrets.FindLast(e => Mathf.Approximately(e.transform.position.x, platformToDelete.transform.position.x) && Mathf.Approximately(e.transform.position.z, platformToDelete.transform.position.z));
                DestroyTurret();
            }
            map[auxXmap, auxZmap] = '0';
            platforms.Remove(platformToDelete);
            
            Destroy(platformToDelete);
            remainingPlatforms++;

        }
    }

    public void setActiveConstructionType(int newSelectedTurret)
    {
        actualSelectedTurret = newSelectedTurret;
        constructTurretFast.SetActive(false);
        constructTurretFire.SetActive(false);
        constructTurretNormal.SetActive(false);
        switch (newSelectedTurret)
        {
            case 2:
                constructTurretNormal.SetActive(true);
                break;
            case 3:
                constructTurretFast.SetActive(true);
                break;
            case 4:
                constructTurretFire.SetActive(true);
                break;
        }
    }

    private void printMap()
    {
        for (int i = 0; i < mapWidth; i++)
        {
            //Debug.Log(auxMap[i, *]);

        }
    }

    private bool tracePath(int actualX, int actualY)
    {
        if (actualX< 0 || actualY< 0 || actualX >= mapWidth || actualY >= mapHeight) {
            return false;
        }

        if (auxMap[actualY,actualX] == BASE) {
            return true;
        }

        if (auxMap[actualY,actualX] != NONE && auxMap[actualY,actualX] != SPAWN) {
            auxMap[actualY, actualX] = AUXFILLER;
            return false;
        }

        auxMap[actualY, actualX] = AUXFILLER;

        if (tracePath(actualX, actualY + 1)) {
            points.Add(new Point(actualX, actualY + 1));
            return true;
        }

        if (tracePath(actualX - 1, actualY)) {
            points.Add(new Point(actualX -1, actualY));
            return true;
        }

        if (tracePath(actualX + 1, actualY)) {
            points.Add(new Point(actualX + 1, actualY));
            return true;
        }

        if (tracePath(actualX, actualY - 1)) {
            points.Add(new Point(actualX, actualY - 1));
            return true;
        }
        return false;
    }

    public bool isValidConstructionSite()
    {
        if (remainingPlatforms > 0)
        {
            if (actualSelectedTurret != 1 && money < 100)
            {
                constructionPlatform.SendMessage("setRed");
                return false;
            }
            points.Clear();
            auxMap = (char[,])map.Clone();
            int auxX = (int)lastPosition.x / 4;
            int auxY = (int)lastPosition.z / 4;
            auxMap[auxX, auxY] = AUXFILLER;            
            auxMap[auxX, auxY] = AUXFILLER;
            if (map[auxX, auxY] == NONE && Mathf.Approximately(lastPosition.y, floorHeight)
                && constructionPlatform.activeSelf
                && tracePath(SPAWNPOSITION.x, SPAWNPOSITION.y))
            {
                constructionPlatform.SendMessage("setBlue");
                return true;
            }
        }
        constructionPlatform.SendMessage("setRed");
        return false;
    }

    void checkCoreHealth()
    {
        if (actualCoreHealth > 0)
            textRoundInformation.text = "Core " + actualCoreHealth + "/" + maxCoreHealth;
        else
            textRoundInformation.text = "You lost";
    }

    public void DamageCore(int amount)
    {
        actualCoreHealth -= amount;
        checkCoreHealth();
    }

    IEnumerator changeToMainMenu(int time = 12)
    {
        yield return new WaitForSeconds(time);
        SceneManager.LoadScene("Menu");
    }

    public void exitGame()
    {
        unPauseGame();
        if (statsUpdater.token != "")
            statsUpdater.updateStats();
        Cursor.visible = true;
        SceneManager.LoadScene("Menu");
    }

    void OnDestroy()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
    }
}
