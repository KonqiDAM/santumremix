﻿using cakeslice;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointinDirecction : MonoBehaviour
{
	RaycastHit hit;
	float maxDistance = 10000;
	[Tooltip("Put Weapon layer and Player layer to ignore bullet raycast.")]
	public LayerMask ignoreLayer;
	Controller cont;

	void Start()
    {
		cont = FindObjectOfType<Controller>();
	}

	// Update is called once per frame
	void Update()
    {
		if (!cont.isConstructingPlatform && !cont.isDeletingPlatform)
		{
			if (Physics.Raycast(transform.position, transform.forward, out hit, maxDistance, ~ignoreLayer))
			{
				if (hit.transform.tag == "Platform" || hit.transform.tag == "Turret")
				{
					hit.transform.SendMessage("On");
					cont.constructionPlatform.SetActive(false);
				}
				else if (hit.transform.tag == "ConstructionPlatform")
				{
					hit.transform.SendMessage("On");
				}
				else if (hit.transform.tag == "LevelPart")
				{
					cont.lastPosition.x = (int)hit.transform.position.x / (int)Controller.tileSize * (int)Controller.tileSize;
					cont.lastPosition.z = (int)hit.transform.position.z / (int)Controller.tileSize * (int)Controller.tileSize;
					cont.lastPosition.y = Controller.floorHeight;
					cont.constructionPlatform.SetActive(true);
					cont.isValidConstructionSite();					
				}
				else
					cont.constructionPlatform.SetActive(false);

				Destroy(gameObject);
			}
			else
				cont.constructionPlatform.SetActive(false);
		}

		Destroy(gameObject, 0.1f);
		cont.CheckConstructionPlatform();
	}
}
