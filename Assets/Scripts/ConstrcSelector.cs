﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConstrcSelector : MonoBehaviour
{
    [SerializeField]
    float timeBetweenChanges = 0.5f;
    float cooldownChange;
    [SerializeField]
    Image platformImg = null;
    [SerializeField]
    Image nTurretImg = null;
    [SerializeField]
    Image fTurretImg = null;
    [SerializeField]
    Image fireTurretImg = null;
    [SerializeField]
    float SelectedTransparency = 1;
    [SerializeField]
    float idleTransparency = 0.7f;
    Color auxColor;
    Image auxSelectedImg;
    int actuallySelected;

    bool isActive = true;

    Controller cont;

    // Start is called before the first frame update
    void Start()
    {
        cooldownChange = timeBetweenChanges;
        cont = FindObjectOfType<Controller>();
        actuallySelected = 1;
        auxColor = platformImg.color;
        auxColor.a = SelectedTransparency;
        platformImg.color = auxColor;
        auxColor.a = idleTransparency;
        nTurretImg.color = auxColor;
        fTurretImg.color = auxColor;
        fireTurretImg.color = auxColor;
        auxSelectedImg = platformImg;
    }

    // Update is called once per frame
    void Update()
    {
        if(cooldownChange > 0)
            cooldownChange -= Time.deltaTime;
        if (isActive && cooldownChange <= 0)
        {

            if (Input.GetAxis("Mouse ScrollWheel") > 0)
            {
                actuallySelected++;
                if (actuallySelected > 4)
                    actuallySelected = 1;
                cooldownChange = timeBetweenChanges;
            }
            else if(Input.GetAxis("Mouse ScrollWheel") < 0)
            {
                actuallySelected--;
                if (actuallySelected < 1)
                    actuallySelected = 4;
                cooldownChange = timeBetweenChanges;
            }
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                actuallySelected = 1;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                actuallySelected = 2;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                actuallySelected = 3;

            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                actuallySelected = 4;
            }
            changeActualSelected(actuallySelected);
        }
    }

    private void changeActualSelected(Image newSelected)
    {
        auxColor.a = idleTransparency;
        auxSelectedImg.color = auxColor;
        auxColor.a = SelectedTransparency;
        newSelected.color = auxColor;
        auxSelectedImg = newSelected;
    }
    private void changeActualSelected(int newSelected)
    {
        cont.actualSelectedTurret = newSelected;
        switch (newSelected)
        {
            case 1:
                changeActualSelected(platformImg);
                break;
            case 2:
                changeActualSelected(nTurretImg);
                break;
            case 3:
                changeActualSelected(fTurretImg);
                break;
            case 4:
                changeActualSelected(fireTurretImg);
                break;
        }
        cont.setActiveConstructionType(newSelected);

    }

    public void setEnabled(bool enabled = true)
    {
        isActive = enabled;
        platformImg.enabled = enabled;
        nTurretImg.enabled = enabled;
        fTurretImg.enabled = enabled;
        fireTurretImg.enabled = enabled;
    }
}
