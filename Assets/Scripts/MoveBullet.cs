﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBullet : MonoBehaviour
{
	[Tooltip("Furthest distance bullet will look for target")]
	public float maxDistance = 1000000;
	
	public GameObject enemy = null;
	bool started = false;
	
	float bulletSpeed = 40;

	void startBullet(GameObject enemy)
	{
		this.enemy = enemy;
		started = true;
	}

	void Update()
	{

		if (started && enemy != null)
		{
			maxDistance -= Time.deltaTime;
			if(maxDistance <= 0)
				Destroy(gameObject);

			RotateToEnemy();
			transform.position = Vector3.MoveTowards(transform.position, enemy.transform.position+enemy.GetComponent<Enemy>().getAimOffset(), bulletSpeed * Time.deltaTime);
		}
		if(started && enemy == null)
			Destroy(gameObject);

	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Dummie")
			other.SendMessage("Hit", 10);
		Destroy(gameObject, 0.01f);
	}

	void RotateToEnemy()
	{
		if (enemy != null)
		{
			Vector3 targetDirection = enemy.transform.position - transform.position;
			this.transform.rotation = Quaternion.LookRotation(targetDirection);
		}
	}
}
