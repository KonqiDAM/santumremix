﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point
{
    public int x;
    public int y;

    public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public Vector3 getVector3()
    {
        return new Vector3(y*Controller.tileSize, Controller.floorHeight, x*Controller.tileSize);
    }

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        return ((Point)obj).x == x && ((Point)obj).y == y;
    }
}
