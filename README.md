# SanctumRemix

## FEATURES:
### Server/DB
* All Node services 
* Login
* Sing up
* Update stats
* List stats
### Game
* Main menu
* Login
* Sing up
* See own stats on main menu
* Start game
* Place 4 types of platforms/turrets
* Destroy platforms
* Only construct on empty spot
* First person view and shoot
* HP UI using post procesing (turning red the screen)
* Some nice post processing effects and enhanced lighting
* Custom sky
* Player can shoot and spawns blood or bullet holes when required
* Enemies
* Enemy spawn
* Administrate waves of enemies to player
* Resources (earn and spend)
* Towers must shoot
* Track stats and save them in DB if user is loged
* General stats screen
* Pause menu
* Cheats (F1 and F2)
* Custom animations
* Nice map with obstacles
## TODO
* Credit screen
* Configuration screen
## Old ideas
* ~~Webpage with global stats~~
* ~~Enemies can atack player~~
* ~~Load levels from file~~
* ~~Load map from files~~
