const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');
const sha256 = require('crypto');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const secretWord = "DAMsecret"
var cors = require('cors');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/santumRemix');

let generateToken = login => {
    let token = jwt.sign({ login: login }, secretWord,
        { expiresIn: "30 days" });
    return token;
}

let validateToken = token => {
    try {
        let result = jwt.verify(token, secretWord);
        return result;
    } catch (e) { }
}

let userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 1,
        trim: true,
        match: /^[a-zA-Z0-9]*$/,
        unique: true
    },
    password: {
        type: String,
        required: true,
        minlength: 4,
        trim: true,
    },
    stats: {
        timePlayed: {
            type: Number,
            required: true
        },
        totalKills: {
            type: Number,
            required: true
        },
        totalDeaths: {
            type: Number,
            required: true
        },
        roundsPlayed: {
            type: Number,
            required: true
        },
        weaponPrecision: {
            type: Number,
            required: true
        },
        totalBulletsUsed: {
            type: Number,
            required: true
        }
    }
});
let User = mongoose.model('users', userSchema);

let app = express();
app.use(cors());
app.use(bodyParser.text({ limit: '200mb' }));
app.use(bodyParser.json({ limit: '500mb'}));
app.use(express.static('public'));

app.get('/stats', (req, res) => {
    User.find().select('-_id -password -__v').then(result => {
        let data = {
            ok: true,
            users: result
        };
        res.send(data);
    })

});
app.post('/register', (req, res) => {
    let newUser = new User({
        name: req.body.name,
        password: req.body.password,
        stats: { 
            totalBulletsUsed: 0,
            totalBulletsUsed: 0,
            weaponPrecision: 1,
            roundsPlayed: 0,
            totalDeaths: 0,
            totalKills: 0,
            timePlayed: 0
        }
    });
    newUser.password = sha256.createHash('sha256').update(newUser.password).digest('hex');
    User.find({ name: newUser.name })
    .then(data => {
        if (data.length > 0) {
            let result = { ok: false, error: "User alredy exists!" };
            res.send(result);
        } else {
            newUser.save().then(result => {
                let data = { ok: true };
                res.send(data);
            }).catch(error => {
                let data = {
                    ok: false,
                    error:error
                };
                res.send(data);
            });
        }
    });
});
app.post('/login', (req, res) => {
    let userLogin = new User({
        name: req.body.name,
        password: sha256.createHash('sha256').update(req.body.password).digest('hex')
    });

    User.findOne({ name: userLogin.name, password: userLogin.password }).select('-_id -password -__v')
        .then(data => {
            // User is valid. Generate token
            if (data) {
                let token = generateToken(userLogin.name);
                let result = { ok: true, token: token, data: data };
                res.send(result);
                // User not found. Generate error message
            } else {
                let result = {
                    ok: false,
                    error: "User or password incorrect"
                };
                res.send(result);
            }
        }).catch(error => {
            // Error searching user. Generate error message
            let result = {
                ok: false,
                error: "User or password incorrect"
            };
            res.send(result);
        });
});
app.put('/stats', (req, res) => {
    let token = req.headers['authorization'];
    let result = validateToken(token);
    if (result) {
        User.findOne({ name: result.login }).then(us => {

            us.stats = {
                totalBulletsUsed: req.body.totalBulletsUsed,
                weaponPrecision: req.body.weaponPrecision,
                roundsPlayed: req.body.roundsPlayed,
                totalDeaths: req.body.totalDeaths,
                totalKills: req.body.totalKills,
                timePlayed: req.body.timePlayed
              }

            us.save().then(result => {
                let data = { ok: true };
                res.send(data);
            }).catch(error => {
                let data = {
                    ok: false,
                    error: "Error updating data: " + error
                };
                res.send(data);
            });
        }).catch(error => {
            let data = {
                ok: false,
                error: "Error updating data: " + us.id
            };
            res.send(data);
        });
    }
    else {
        res.sendStatus(401)
    }
});
app.listen(3000);